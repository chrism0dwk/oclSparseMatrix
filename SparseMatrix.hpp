/////////////////////////////////////////////////////////////
// Name: SparseMatrix.hpp				   //
// Created: 2015-07-22					   //
// Author: Chris Jewell <c.jewell@lancaster.ac.uk>	   //
// Copyright: (c) Chris Jewell 2015			   //
// Purpose: OpenCL implementation of a CRS distance matrix //
/////////////////////////////////////////////////////////////

#ifndef SPARSEMATRIX_HPP
#define SPARSEMATRIX_HPP

#ifndef __CL_ENABLE_EXCEPTIONS
#define __CL_ENABLE_EXCEPTIONS
#endif

#include "cl.hpp"
#include "SparseMatrix.cl"

#define THREADSPERBLOCK 128
typedef cl_float2 Point2D;

//! Calculates the number of pairs of non-overlapping coordinates within dLimit of each other
cl_ulong
numDWithin(cl::CommandQueue& queue, const Point2D* coords, const size_t n, const float dLimit);


#endif
