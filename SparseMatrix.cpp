/////////////////////////////////////////////////////////////
// Name: SparseMatrix.hpp				   //
// Created: 2015-07-22					   //
// Author: Chris Jewell <c.jewell@lancaster.ac.uk>	   //
// Copyright: (c) Chris Jewell 2015			   //
// Purpose: OpenCL implementation of a CRS distance matrix //
/////////////////////////////////////////////////////////////

#include "SparseMatrix.hpp"

#include <functional>
#include <iostream>
#include <numeric>

#define NUMCHUNKS 64

//! Allocates a 2D column-major matrix
inline size_t clMemPitch(const cl::Device device, const size_t size)
{
  size_t addrAlign = device.getInfo<CL_DEVICE_MEM_BASE_ADDR_ALIGN>();
  return (size + addrAlign - 1) / addrAlign;
}



cl_ulong
numDWithin(cl::CommandQueue& queue, const Point2D* coords, const size_t n, const float dLimit)
{
  // CL
  const cl::Context context(queue.getInfo<CL_QUEUE_CONTEXT>()); 
  cl::Device device = queue.getInfo<CL_QUEUE_DEVICE>();

  // Get Grid dimensions
  cl::NDRange groupsize(THREADSPERBLOCK,1);
  size_t xgroups = (n + THREADSPERBLOCK - 1) / THREADSPERBLOCK;
  size_t ygroups = (n + THREADSPERBLOCK* NUMCHUNKS-1)/(THREADSPERBLOCK*NUMCHUNKS);
  cl::NDRange globalsize(xgroups*THREADSPERBLOCK, ygroups);

  // Create buffers
  cl::Buffer devCoords(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, sizeof(Point2D)*n, (void*)coords);
  cl::Buffer nnz(context, CL_MEM_READ_WRITE, sizeof(cl_ulong));

  
  // Build program
  cl::Program program;
  try {
    cl::Program::Sources sources;
    sources.push_back({sparseMatrixCL.c_str(), sparseMatrixCL.length()});
    program = cl::Program(context, sources);
    program.build({device});
  }
  catch (cl::Error& e) {
    throw std::runtime_error("Error building program: " + program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device));
  }

  // Construct kernels
  cl::Kernel numDWithin;
  cl::Kernel zero;
  try {
    zero = cl::Kernel(program, "zero");
    zero.setArg(0, nnz);
    zero.setArg(1, sizeof(cl_ulong));
    numDWithin = cl::Kernel(program, "numDWithin");
    numDWithin.setArg(0, devCoords);
    numDWithin.setArg(1, n);
    numDWithin.setArg(2, dLimit);
    numDWithin.setArg(3, nnz);
    numDWithin.setArg(4, NUMCHUNKS);
  }
  catch (cl::Error& e) {
    throw std::runtime_error("Could not compile kernel");
  }

  std::cout << "numDWithin kernel work group size: " << numDWithin.getWorkGroupInfo<CL_KERNEL_WORK_GROUP_SIZE>(device) << std::endl;
  // Enqueue kernel
  std::cout << "Workgroup size (" << groupsize[0] << "," << groupsize[1] << ")" << std::endl;
  std::cout << "Global size (" << globalsize[0] << "," << globalsize[1] << ")" << std::endl;


  cl::Event calcEvent;
  try {
    queue.enqueueNDRangeKernel(zero, cl::NullRange, cl::NDRange(sizeof(cl_ulong)));
    queue.enqueueNDRangeKernel(numDWithin, cl::NullRange, globalsize, groupsize, NULL, &calcEvent);
  }
  catch (cl::Error& e) {
    std::cerr << "Exception launching kernel: " << e.what() << ": " << e.err() << std::endl;
    throw e;
  }
  
  // Get data back
  cl_ulong hostNNZ;
  queue.enqueueReadBuffer(nnz, CL_TRUE, 0, sizeof(cl_ulong), &hostNNZ);
    
  // Reduction on the host
  cl_ulong rv = hostNNZ; //std::accumulate(hostNNZ.begin(), hostNNZ.end(), 0ull);

  // Timing
  cl_ulong start,end;
  double time;
  calcEvent.wait();
  calcEvent.getProfilingInfo(CL_PROFILING_COMMAND_START, &start);
  calcEvent.getProfilingInfo(CL_PROFILING_COMMAND_END, &end);
  time = (end - start)/1.0e6;
  std::cout << "Kernel time: " << time << std::endl;
  return rv;
}
