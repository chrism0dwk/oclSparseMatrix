/////////////////////////////////////////////////////////////
// Name: SparseMatrix.hpp				   //
// Created: 2015-07-22					   //
// Author: Chris Jewell <c.jewell@lancaster.ac.uk>	   //
// Copyright: (c) Chris Jewell 2015			   //
// Purpose: OpenCL implementation of a CRS distance matrix //
/////////////////////////////////////////////////////////////

#define STRINGIFY(x) #x
#include <string>
#include <cassert>

#pragma OPENCL EXTENSION cl_khr_int64_base_atomics : enable
const std::string sparseMatrixCL(STRINGIFY(

					   //! Zeros out a 1D data structure
					   __kernel
					   void
					   zero(__global char* buff, const ulong n)
					   {
					     buff[get_global_id(0)] = 0;
					   }
					   
					   //! Sum reduces the 1D buffer in local memory
					   __kernel
					   void
					   shmemReduce(__local ulong* buff)
					   {
					     barrier(CLK_LOCAL_MEM_FENCE);

					     size_t tid = get_local_id(0);
  
					     for(size_t size = get_local_size(0) / 2; size > 0; size >>= 1)
					       {
						 if (tid < size)
						   buff[tid] += buff[tid + size];
						 barrier(CLK_LOCAL_MEM_FENCE);
					       }
					     // if(tid < 32) // Not guaranteed to work if 32 threads aren't in sync
					     //   {
					     // 	 volatile __local int* vbuff = buff;
					     // 	 vbuff[tid] += vbuff[tid + 32];
					     // 	 vbuff[tid] += vbuff[tid + 16];
					     // 	 vbuff[tid] += vbuff[tid + 8 ];
					     // 	 vbuff[tid] += vbuff[tid + 4 ];
					     // 	 vbuff[tid] += vbuff[tid + 2 ];
					     // 	 vbuff[tid] += vbuff[tid + 1 ];
					     //   }
					     //barrier(CLK_LOCAL_MEM_FENCE);
					   }


					   //! Computes number of pairs of coordinates within distance.
					   //
					   //! @param coords an nx2 column major array of 2D coordinates
					   //! @param n the number of coordinates (i.e. rows) of coords
					   //! @param pitch the row pitch of coords
					   //! @param distance the distance limit
					   //! @param output the output array for final reduction
					   //! @param ybuff size of the local memory to use (get_local_size(0)*2)
					   //! @param nnzbuff size of local memory buffer (get_local_size(0))
					   __kernel
					   void
					   numDWithin(__global const float2* coords, const ulong n,
						      const float distance, __global ulong* output, int nChunks)
					   {

					     __local float2 ybuff[128];
					     __local ulong nnzbuff[128];

					     // Get global row/col
					     ulong col = get_global_id(0);
					     int lcol = get_local_id(0);
					     
					     float dsq = distance * distance;
    
					     nnzbuff[lcol] = 0ul;
					     if (col < n)
					       {
					     	 ybuff[lcol] = coords[col]; // X coords
					       }
    
					     barrier(CLK_LOCAL_MEM_FENCE);

					     for(int chunk=0; chunk<nChunks; ++chunk)
					     {
					       ulong row = (get_group_id(1)*get_local_size(0)*nChunks) + (get_local_size(0)*chunk) + get_local_id(0);
						 if (row < n)
						   {
						     float2 x = coords[row];
						     int collimit = min((ulong)get_local_size(0), (ulong)(n - get_group_id(0)*get_local_size(0)));
						     for (int mycol = 0; mycol < collimit; mycol++)
						       {
							 float2 y = ybuff[mycol];
							 float2 diff = x - y;
							 float d = diff.x * diff.x + diff.y * diff.y;
							 nnzbuff[lcol] += (d <= dsq) & (d > 0.0f);
						       }
						   }
					     }
					      shmemReduce(nnzbuff);
					      if(get_local_id(0) == 0ul & get_local_id(1) == 0ul)
						{
						  //ulong blkIdx = get_group_id(0) + get_group_id(1)*get_num_groups(0);
						  //output[blkIdx] = nnzbuff[0];
						  atom_add(output, nnzbuff[0]);
						}
					   }

					   __kernel
					   void
					   testAddReduce(const ulong size,__global ulong* output)
					   {
					     __local ulong buff[128];
					     buff[get_local_id(0)] = 1;
					     shmemReduce(buff);
					     output[get_group_id(0)] = buff[0];
					   }
					   ));
