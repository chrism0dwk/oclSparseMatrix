/////////////////////////////////////////////////////////////
// Name: test.cpp        				   //
// Created: 2015-07-23					   //
// Author: Chris Jewell <c.jewell@lancaster.ac.uk>	   //
// Copyright: (c) Chris Jewell 2015			   //
// Purpose: Tests SparseMatrix.cl                          //
/////////////////////////////////////////////////////////////

#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <stdexcept>

#include "SparseMatrix.hpp"

void
importData(const std::string filename, std::vector<Point2D>& data)
{

  std::ifstream file(filename);
  std::string buff;
  file >> buff; // Strip header
  while(file.good()) {
    buff.clear();
    file >> buff;
    size_t comma = buff.find(",");
    if (comma == std::string::npos) break;
    float x,y;
    x = std::stof(buff.substr(0,comma));
    y = std::stof(buff.substr(comma+1));
    data.push_back({x,y});
  }
}

cl::Device
chooseCLDevice(const cl::Platform& platform)
{
    // Fetch CPU device
  std::cerr << "Checking CPU..." << std::flush;
  std::vector<cl::Device> cpuDevices;
  try {
    platform.getDevices(CL_DEVICE_TYPE_CPU, &cpuDevices);
    std::cerr << "found " << cpuDevices.size() << std::endl;
  } catch(cl::Error& e) {
    std::cerr << "failed. ";
    if(e.err() == CL_DEVICE_NOT_FOUND) std::cerr << "No devices present." << std::endl;
  }

  // Fetch GPU devices
  std::cerr << "Checking GPU..." << std::flush;
  std::vector< cl::Device> gpuDevices;
  try {
    platform.getDevices(CL_DEVICE_TYPE_GPU, &gpuDevices);
    std::cerr << "found " << gpuDevices.size() << std::endl;
  }
  catch(cl::Error& e) {
    std::cerr << "failed. ";
    if(e.err() == CL_DEVICE_NOT_FOUND) std::cerr << "No devices present." << std::endl;
  }

  // Stack devices
  std::vector<cl::Device> allDevices = gpuDevices;
  allDevices.insert(allDevices.end(), cpuDevices.begin(), cpuDevices.end());
  
  if(allDevices.size() == 0)
    throw std::runtime_error("No OpenCL compatible devices found. Exitting.");

  // Get with > 2000GB memory.  Order of devices chooses GPU first, CPU second
  cl::Device device;
  for(size_t i=0; i<allDevices.size(); ++i)
    {
      if(allDevices[i].getInfo<CL_DEVICE_GLOBAL_MEM_SIZE>() >= 2000*1024*1024) {
        device = allDevices[i];
        break;
      }
    }
  
  return device;
}

  

int main(int argc, char* argv[])
{

  if(argc != 2) {
    std::cerr << "Usage: test <coords file>" << std::endl;
    exit(EXIT_FAILURE);
  }

  std::string filename(argv[1]);

  // Read in data
  std::vector<Point2D> points;
  try {
    importData(filename, points);
  }
  catch(std::ifstream::failure& e) {
    std::cerr << "Could not open input file '" << filename << "': " << e.what() << std::endl;
    exit(EXIT_FAILURE); 
  }

  std::cout << "Data length: " << points.size() << std::endl;
  

  // Set up OpenCL here
  std::vector<cl::Platform> platformList;
    try {
    cl::Platform::get(&platformList);
  }
  catch(cl::Error& e) {
    std::cerr << "No OpenCL platform found.  Exitting." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  cl::Platform platform = platformList[0];
  std::cout << "Using platform: " << platform.getInfo<CL_PLATFORM_NAME>() << std::endl;

  cl::Device device = chooseCLDevice(platform);

  // Set up context
  cl::Context context(device);

  std::cout << "Using device: " << device.getInfo<CL_DEVICE_NAME>() << std::endl;
  std::cout << "Max workgroup size: " << device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() << std::endl;
  
  std::vector<size_t> maxWorkItemSizes = device.getInfo<CL_DEVICE_MAX_WORK_ITEM_SIZES>();
  std::cout << "Max work item sizes: (" << maxWorkItemSizes[0];
  for(int i=1; i<device.getInfo<CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS>(); ++i) std::cout << "," << maxWorkItemSizes[i];
  std::cout << ")" << std::endl;

  // Set up queue
  cl::CommandQueue queue(context, device, CL_QUEUE_PROFILING_ENABLE);

  // Do distance calculation
  cl_ulong nnz;
  try {
    nnz = numDWithin(queue, points.data(), points.size(), 100.0f);
  }
  catch(cl::Error& e) {
    std::cerr << "cl::Error caught: " << e.what() << ": " << e.err() << std::endl;
    exit(EXIT_FAILURE);
  }

  //std::cout << "NNZ: " << nnz << std::endl;
  std::cout << "sizeof(cl_ulong): " << sizeof(cl_ulong) << " bytes" << std::endl;
  printf("NNZ: %llu\n", nnz);
  return EXIT_SUCCESS;
}
