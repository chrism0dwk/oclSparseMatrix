PLATFORM=$(shell uname)

OBJ = SparseMatrix.o test.o
PKG_CXXFLAGS=-O0 -g -std=c++11

all: test

test: $(OBJ)
ifeq ($(PLATFORM),Darwin)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(PKG_CXXFLAGS) -framework OpenCL -o $@ $(OBJ) $(LDFLAGS)
else
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(PKG_CXXFLAGS) -o $@ $(OBJ) $(LDFLAGS) -lOpenCL
endif

.cpp.o:
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(PKG_CXXFLAGS) -c $< -o $@

clean:
	rm -rf *.o test
